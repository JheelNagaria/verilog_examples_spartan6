
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name example4.7.3 -dir "E:/Jheel/verilog_examples_spartan6/example4.7.3/planAhead_run_1" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/verilog_examples_spartan6/example4.7.3/rotat_square.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/verilog_examples_spartan6/example4.7.3} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "rotat_square.ucf" [current_fileset -constrset]
add_files [list {rotat_square.ucf}] -fileset [get_property constrset [current_run]]
link_design
