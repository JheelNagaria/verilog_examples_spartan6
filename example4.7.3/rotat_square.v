`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:18:45 10/12/2018 
// Design Name: 
// Module Name:    rotat_square 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rotat_square(
    input wire clk,
    output reg [3:0] anode,
    output reg [6:0] cathode
    );
	 
	 reg [26:0] counter = 0;
	 wire one_sec_clk;
	 reg [1:0] anode_current = 2'b00, anode_next = 2'b00;
	 reg cathode_current = 1'b1, cathode_next = 1'b1;
	 
	 //body
	 always @(posedge clk)
	 begin
		
		counter = counter + 1;
		anode_current <= anode_next;
		cathode_current <= cathode_next;
		
		case (anode_current)
			2'b00: anode = 4'b1110;
			2'b01: anode = 4'b1101;
			2'b10: anode = 4'b1011;
			2'b11: anode = 4'b0111;
		endcase
		
		cathode = (cathode_current == 1'b1)? 7'b0011100 : 7'b1100010;
		
	 end
	 
	 assign one_sec_clk = (counter == 100000000)? 1'b1: 1'b0;
	 
	 
	 //next state logic
	 always @(posedge one_sec_clk)
	 begin
		
		if (cathode_current == 1'b1) begin
			
			cathode_next = (anode_current == 2'b11)? 1'b0: 1'b1;
			anode_next = (anode_current == 2'b11)? anode_current:(anode_current + 2'b01);
		
		end
		else begin
			
			cathode_next = (anode_current == 0)? 1'b1: 1'b0;
			anode_next = (anode_current == 0)? anode_current: (anode_current - 2'b01);
		
		end
	 
	 end
	 


endmodule
