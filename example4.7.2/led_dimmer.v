`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:36:03 10/11/2018 
// Design Name: 
// Module Name:    led_dimmer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module led_dimmer(
    input clk_100MHz,
    input [3:0] w,
    output reg anode,
	 output reg [7:0] cathode
    );
	 
	 reg [25:0] count = 0;
	 wire [25:0] ref = (w * 100000)/16;
	 
	 always @(posedge clk_100MHz)
	 begin
		count = count + 1;
		cathode <= 8'b00000000;
	 
		if (count < 100000) begin	
			if (count < ref)
				anode <= 1'b1;
			else anode <= 1'b0;
		end
		else count = 0;
		
	 end
	 
	 


endmodule
