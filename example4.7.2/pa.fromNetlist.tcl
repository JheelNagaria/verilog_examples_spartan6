
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name example4.7.2 -dir "E:/Jheel/verilog_examples_spartan6/example4.7.2/planAhead_run_2" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/verilog_examples_spartan6/example4.7.2/led_dimmer.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/verilog_examples_spartan6/example4.7.2} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "pwm_gen.ucf" [current_fileset -constrset]
add_files [list {pwm_gen.ucf}] -fileset [get_property constrset [current_run]]
link_design
