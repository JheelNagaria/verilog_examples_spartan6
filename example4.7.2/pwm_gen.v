`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:30:30 10/10/2018 
// Design Name: 
// Module Name:    pwm_gen 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module pwm_gen(
	 input clk_100MHz,
    input [3:0] w,
    output reg pwm_out
    );
	 
	 reg [25:0] count = 0;
	 wire [25:0] ref = (w * 100000)/16;
	 
	 always @(posedge clk_100MHz)
	 begin
		count = count + 1;
	 
		if (count < 100000) begin	
			if (count < ref)
				pwm_out <= 1'b1;
			else pwm_out <= 1'b0;
		end
		else count = 0;
		
	 end
	 

endmodule
