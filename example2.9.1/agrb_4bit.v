`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:33:06 09/19/2018 
// Design Name: 
// Module Name:    agrb_4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module agrb_4bit(
    input wire [3:0] a,
    input wire [3:0] b,
    output wire agrb_4
    );
	 
	 wire a1a0grb1b0, a3a2grb3b2, a3a2eqb3b2;
	 
	 aeqb_2bit a3a2eqb3b2_ (.a({a[3], a[2]}), .b({b[3], b[2]}), .aeqb_2(a3a2eqb3b2));
	 agrb_2bit a1a0grb1b0_ (.a({a[1], a[0]}), .b({b[1], b[0]}), .agrb_2(a1a0grb1b0));
	 agrb_2bit a3a2grb3b2_ (.a({a[3], a[2]}), .b({b[3], b[2]}), .agrb_2(a3a2grb3b2));
	 
	 assign agrb_4 = (a1a0grb1b0 & a3a2eqb3b2) | a3a2grb3b2;

endmodule
