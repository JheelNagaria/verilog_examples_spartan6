`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:29:07 09/19/2018 
// Design Name: 
// Module Name:    aeq_2bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module aeqb_2bit(
    input wire [1:0] a,
    input wire [1:0] b,
    output wire aeqb_2
    );
	 
	 wire e0, e1;
	 
	 eq eq0(.a(a[0]), .b(b[0]), .aeqb(e0));
	 eq eq1(.a(a[1]), .b(b[1]), .aeqb(e1));
	 
	 assign aeqb_2 = e0 & e1;


endmodule


module eq(
	 input wire a,
	 input wire b,
	 output wire aeqb
	 );
	 
	 assign aeqb = (~a & ~b) | (a & b);
endmodule
