`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:49:16 09/19/2018 
// Design Name: 
// Module Name:    agrb_2bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module agrb_2bit(
    input wire [1:0] a,
    input wire [1:0] b,
    output wire agrb_2
    );
	 
	 wire a0grb0, a1grb1, a1eqb1;
	 
	 gr a0grb0_ (.a(a[0]), .b(b[0]), .agrb(a0grb0));
	 gr a1grb1_ (.a(a[1]), .b(b[1]), .agrb(a1grb1));
	 eq a1eqb1_ (.a(a[1]), .b(b[1]), .aeqb(a1eqb1));
	 
	 assign agrb_2 = (a0grb0 & a1eqb1) | a1grb1;

endmodule


module gr(
	 input wire a,
	 input wire b,
	 output wire agrb
	 );
	 
	 assign agrb = a & ~b;

endmodule