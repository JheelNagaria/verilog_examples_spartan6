
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name example2.9.1 -dir "E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1/planAhead_run_1" -part xc6slx16csg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1/agrb_2bit_constr.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {agrb_2bit.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top agrb_2bit $srcset
add_files [list {agrb_2bit_constr.ucf}] -fileset [get_property constrset [current_run]]
add_files [list {agrb_2bit.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx16csg324-3
