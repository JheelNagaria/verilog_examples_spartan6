/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000001758407609_0230603978_init();
    work_m_00000000001105736190_0955705880_init();
    work_m_00000000000947952668_2793508850_init();
    work_m_00000000000546220397_1262221304_init();
    work_m_00000000000160964645_1850779684_init();
    work_m_00000000000105619373_0626862940_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000000105619373_0626862940");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
