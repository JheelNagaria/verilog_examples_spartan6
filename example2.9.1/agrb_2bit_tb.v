`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   10:55:46 09/19/2018
// Design Name:   agrb_2bit
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1/agrb_2bit_tb.v
// Project Name:  example2.9.1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: agrb_2bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module agrb_2bit_tb;

	// Inputs
	reg [1:0] test_a;
	reg [1:0] test_b;

	// Outputs
	wire test_agrb_2;

	// Instantiate the Unit Under Test (UUT)
	agrb_2bit uut (.a(test_a), .b(test_b), .agrb_2(test_agrb_2));

	initial begin
		// Initialize Inputs
		test_a = 2'b00;
		test_b = 2'b00;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		test_a = 2'b00;
		test_b = 2'b01;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b00;
		test_b = 2'b10;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b00;
		test_b = 2'b11;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b01;
		test_b = 2'b00;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b01;
		test_b = 2'b01;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b01;
		test_b = 2'b10;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b01;
		test_b = 2'b11;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b10;
		test_b = 2'b00;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b10;
		test_b = 2'b01;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b10;
		test_b = 2'b10;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b10;
		test_b = 2'b11;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b11;
		test_b = 2'b00;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b11;
		test_b = 2'b01;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b11;
		test_b = 2'b10;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_a = 2'b11;
		test_b = 2'b11;

		// Wait 100 ns for global reset to finish
		#100;
		
		$stop;

	end
      
endmodule

