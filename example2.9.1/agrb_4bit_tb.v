`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:51:18 09/19/2018
// Design Name:   agrb_4bit
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1/agrb_4bit_tb.v
// Project Name:  example2.9.1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: agrb_4bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module agrb_4bit_tb;

	// Inputs
	reg [3:0] test_a;
	reg [3:0] test_b;

	// Outputs
	wire test_agrb_4;
	integer i = 0, j = 0;
	// Instantiate the Unit Under Test (UUT)
	agrb_4bit uut (.a(test_a), .b(test_b), .agrb_4(test_agrb_4));

	initial begin
		// Initialize Inputs
		test_a = 0;
		test_b = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		for (i = 0; i < 16; i = i + 1) begin
			for (j = 0; j < 16; j = j + 1) begin
				test_a = i;
				test_b = j;
				
				#100;
			end
		end
		$stop;

	end
      
endmodule

