
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name example2.9.1 -dir "E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1/planAhead_run_4" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1/agrb_4bit.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.1} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "agrb_2bit.ucf" [current_fileset -constrset]
add_files [list {agrb_2bit.ucf}] -fileset [get_property constrset [current_run]]
link_design
