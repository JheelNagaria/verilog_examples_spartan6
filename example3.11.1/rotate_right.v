`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:54:28 09/20/2018 
// Design Name: 
// Module Name:    rotate_right 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rotate_right(
    input wire [7:0] in,
    output reg [7:0] out
    );
	 
	 always @(in)
	 begin
		out = {in[0], in[7:1]};
	 end

endmodule
