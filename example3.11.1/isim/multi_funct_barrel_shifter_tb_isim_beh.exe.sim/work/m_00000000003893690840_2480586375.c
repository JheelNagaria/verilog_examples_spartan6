/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.1/multi_func_barrel_shifter.v";
static unsigned int ng1[] = {0U, 0U};
static unsigned int ng2[] = {1U, 0U};
static unsigned int ng3[] = {2U, 0U};
static unsigned int ng4[] = {3U, 0U};
static unsigned int ng5[] = {4U, 0U};
static unsigned int ng6[] = {5U, 0U};
static unsigned int ng7[] = {6U, 0U};



static void Always_34_0(char *t0)
{
    char t16[8];
    char t17[8];
    char t19[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    char *t11;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    unsigned int t18;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    char *t26;
    char *t27;

LAB0:    t1 = (t0 + 3168U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(34, ng0);
    t2 = (t0 + 3488);
    *((int *)t2) = 1;
    t3 = (t0 + 3200);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(35, ng0);

LAB5:    xsi_set_current_line(37, ng0);
    t4 = (t0 + 1208U);
    t5 = *((char **)t4);
    t4 = (t5 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (~(t6));
    t8 = *((unsigned int *)t5);
    t9 = (t8 & t7);
    t10 = (t9 != 0);
    if (t10 > 0)
        goto LAB6;

LAB7:    xsi_set_current_line(54, ng0);

LAB28:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 1368U);
    t3 = *((char **)t2);

LAB29:    t2 = ((char*)((ng1)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB30;

LAB31:    t2 = ((char*)((ng2)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB32;

LAB33:    t2 = ((char*)((ng3)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB34;

LAB35:    t2 = ((char*)((ng4)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB36;

LAB37:    t2 = ((char*)((ng5)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB38;

LAB39:    t2 = ((char*)((ng6)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB40;

LAB41:    t2 = ((char*)((ng7)));
    t13 = xsi_vlog_unsigned_case_compare(t3, 3, t2, 3);
    if (t13 == 1)
        goto LAB42;

LAB43:
LAB45:
LAB44:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 1048U);
    t4 = *((char **)t2);
    memset(t17, 0, 8);
    t2 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 1);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 1);
    *((unsigned int *)t2) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 127U);
    t18 = *((unsigned int *)t2);
    *((unsigned int *)t2) = (t18 & 127U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    t22 = (t21 & 1);
    *((unsigned int *)t19) = t22;
    t23 = *((unsigned int *)t15);
    t24 = (t23 >> 0);
    t25 = (t24 & 1);
    *((unsigned int *)t11) = t25;
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 1, t17, 7);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);

LAB46:
LAB8:    goto LAB2;

LAB6:    xsi_set_current_line(38, ng0);

LAB9:    xsi_set_current_line(40, ng0);
    t11 = (t0 + 1368U);
    t12 = *((char **)t11);

LAB10:    t11 = ((char*)((ng1)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t11, 3);
    if (t13 == 1)
        goto LAB11;

LAB12:    t2 = ((char*)((ng2)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t2, 3);
    if (t13 == 1)
        goto LAB13;

LAB14:    t2 = ((char*)((ng3)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t2, 3);
    if (t13 == 1)
        goto LAB15;

LAB16:    t2 = ((char*)((ng4)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t2, 3);
    if (t13 == 1)
        goto LAB17;

LAB18:    t2 = ((char*)((ng5)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t2, 3);
    if (t13 == 1)
        goto LAB19;

LAB20:    t2 = ((char*)((ng6)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t2, 3);
    if (t13 == 1)
        goto LAB21;

LAB22:    t2 = ((char*)((ng7)));
    t13 = xsi_vlog_unsigned_case_compare(t12, 3, t2, 3);
    if (t13 == 1)
        goto LAB23;

LAB24:
LAB26:
LAB25:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 1048U);
    t3 = *((char **)t2);
    memset(t17, 0, 8);
    t2 = (t17 + 4);
    t4 = (t3 + 4);
    t6 = *((unsigned int *)t3);
    t7 = (t6 >> 7);
    t8 = (t7 & 1);
    *((unsigned int *)t17) = t8;
    t9 = *((unsigned int *)t4);
    t10 = (t9 >> 7);
    t18 = (t10 & 1);
    *((unsigned int *)t2) = t18;
    t5 = (t0 + 1048U);
    t11 = *((char **)t5);
    memset(t19, 0, 8);
    t5 = (t19 + 4);
    t14 = (t11 + 4);
    t20 = *((unsigned int *)t11);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t14);
    t23 = (t22 >> 0);
    *((unsigned int *)t5) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 127U);
    t25 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t25 & 127U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 7, t17, 1);
    t15 = (t0 + 1928);
    xsi_vlogvar_assign_value(t15, t16, 0, 0, 8);

LAB27:    goto LAB8;

LAB11:    xsi_set_current_line(41, ng0);
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    t14 = (t0 + 1928);
    xsi_vlogvar_assign_value(t14, t15, 0, 0, 8);
    goto LAB27;

LAB13:    xsi_set_current_line(42, ng0);
    t3 = (t0 + 1048U);
    t4 = *((char **)t3);
    memset(t17, 0, 8);
    t3 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 1);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 1);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 127U);
    t18 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t18 & 127U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    t22 = (t21 & 1);
    *((unsigned int *)t19) = t22;
    t23 = *((unsigned int *)t15);
    t24 = (t23 >> 0);
    t25 = (t24 & 1);
    *((unsigned int *)t11) = t25;
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 1, t17, 7);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);
    goto LAB27;

LAB15:    xsi_set_current_line(43, ng0);
    t3 = (t0 + 1048U);
    t4 = *((char **)t3);
    memset(t17, 0, 8);
    t3 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 2);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 2);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 63U);
    t18 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t18 & 63U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t15);
    t23 = (t22 >> 0);
    *((unsigned int *)t11) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 3U);
    t25 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t25 & 3U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 2, t17, 6);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);
    goto LAB27;

LAB17:    xsi_set_current_line(44, ng0);
    t3 = (t0 + 1048U);
    t4 = *((char **)t3);
    memset(t17, 0, 8);
    t3 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 3);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 3);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 31U);
    t18 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t18 & 31U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t15);
    t23 = (t22 >> 0);
    *((unsigned int *)t11) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 7U);
    t25 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t25 & 7U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 3, t17, 5);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);
    goto LAB27;

LAB19:    xsi_set_current_line(45, ng0);
    t3 = (t0 + 1048U);
    t4 = *((char **)t3);
    memset(t17, 0, 8);
    t3 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 4);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 4);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 15U);
    t18 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t18 & 15U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t15);
    t23 = (t22 >> 0);
    *((unsigned int *)t11) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 15U);
    t25 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t25 & 15U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 4, t17, 4);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);
    goto LAB27;

LAB21:    xsi_set_current_line(46, ng0);
    t3 = (t0 + 1048U);
    t4 = *((char **)t3);
    memset(t17, 0, 8);
    t3 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 5);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 5);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 7U);
    t18 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t18 & 7U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t15);
    t23 = (t22 >> 0);
    *((unsigned int *)t11) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 31U);
    t25 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t25 & 31U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 5, t17, 3);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);
    goto LAB27;

LAB23:    xsi_set_current_line(47, ng0);
    t3 = (t0 + 1048U);
    t4 = *((char **)t3);
    memset(t17, 0, 8);
    t3 = (t17 + 4);
    t5 = (t4 + 4);
    t6 = *((unsigned int *)t4);
    t7 = (t6 >> 6);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 6);
    *((unsigned int *)t3) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 3U);
    t18 = *((unsigned int *)t3);
    *((unsigned int *)t3) = (t18 & 3U);
    t11 = (t0 + 1048U);
    t14 = *((char **)t11);
    memset(t19, 0, 8);
    t11 = (t19 + 4);
    t15 = (t14 + 4);
    t20 = *((unsigned int *)t14);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t15);
    t23 = (t22 >> 0);
    *((unsigned int *)t11) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 63U);
    t25 = *((unsigned int *)t11);
    *((unsigned int *)t11) = (t25 & 63U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 6, t17, 2);
    t26 = (t0 + 1928);
    xsi_vlogvar_assign_value(t26, t16, 0, 0, 8);
    goto LAB27;

LAB30:    xsi_set_current_line(57, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    t4 = (t0 + 1928);
    xsi_vlogvar_assign_value(t4, t5, 0, 0, 8);
    goto LAB46;

LAB32:    xsi_set_current_line(58, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t17, 0, 8);
    t4 = (t17 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 7);
    t8 = (t7 & 1);
    *((unsigned int *)t17) = t8;
    t9 = *((unsigned int *)t11);
    t10 = (t9 >> 7);
    t18 = (t10 & 1);
    *((unsigned int *)t4) = t18;
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    memset(t19, 0, 8);
    t14 = (t19 + 4);
    t26 = (t15 + 4);
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t26);
    t23 = (t22 >> 0);
    *((unsigned int *)t14) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 127U);
    t25 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t25 & 127U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 7, t17, 1);
    t27 = (t0 + 1928);
    xsi_vlogvar_assign_value(t27, t16, 0, 0, 8);
    goto LAB46;

LAB34:    xsi_set_current_line(59, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t17, 0, 8);
    t4 = (t17 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 6);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t11);
    t9 = (t8 >> 6);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 3U);
    t18 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t18 & 3U);
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    memset(t19, 0, 8);
    t14 = (t19 + 4);
    t26 = (t15 + 4);
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t26);
    t23 = (t22 >> 0);
    *((unsigned int *)t14) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 63U);
    t25 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t25 & 63U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 6, t17, 2);
    t27 = (t0 + 1928);
    xsi_vlogvar_assign_value(t27, t16, 0, 0, 8);
    goto LAB46;

LAB36:    xsi_set_current_line(60, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t17, 0, 8);
    t4 = (t17 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 5);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t11);
    t9 = (t8 >> 5);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 7U);
    t18 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t18 & 7U);
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    memset(t19, 0, 8);
    t14 = (t19 + 4);
    t26 = (t15 + 4);
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t26);
    t23 = (t22 >> 0);
    *((unsigned int *)t14) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 31U);
    t25 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t25 & 31U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 5, t17, 3);
    t27 = (t0 + 1928);
    xsi_vlogvar_assign_value(t27, t16, 0, 0, 8);
    goto LAB46;

LAB38:    xsi_set_current_line(61, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t17, 0, 8);
    t4 = (t17 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 4);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t11);
    t9 = (t8 >> 4);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 15U);
    t18 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t18 & 15U);
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    memset(t19, 0, 8);
    t14 = (t19 + 4);
    t26 = (t15 + 4);
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t26);
    t23 = (t22 >> 0);
    *((unsigned int *)t14) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 15U);
    t25 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t25 & 15U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 4, t17, 4);
    t27 = (t0 + 1928);
    xsi_vlogvar_assign_value(t27, t16, 0, 0, 8);
    goto LAB46;

LAB40:    xsi_set_current_line(62, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t17, 0, 8);
    t4 = (t17 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 3);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t11);
    t9 = (t8 >> 3);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 31U);
    t18 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t18 & 31U);
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    memset(t19, 0, 8);
    t14 = (t19 + 4);
    t26 = (t15 + 4);
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t26);
    t23 = (t22 >> 0);
    *((unsigned int *)t14) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 7U);
    t25 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t25 & 7U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 3, t17, 5);
    t27 = (t0 + 1928);
    xsi_vlogvar_assign_value(t27, t16, 0, 0, 8);
    goto LAB46;

LAB42:    xsi_set_current_line(63, ng0);
    t4 = (t0 + 1048U);
    t5 = *((char **)t4);
    memset(t17, 0, 8);
    t4 = (t17 + 4);
    t11 = (t5 + 4);
    t6 = *((unsigned int *)t5);
    t7 = (t6 >> 2);
    *((unsigned int *)t17) = t7;
    t8 = *((unsigned int *)t11);
    t9 = (t8 >> 2);
    *((unsigned int *)t4) = t9;
    t10 = *((unsigned int *)t17);
    *((unsigned int *)t17) = (t10 & 63U);
    t18 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t18 & 63U);
    t14 = (t0 + 1048U);
    t15 = *((char **)t14);
    memset(t19, 0, 8);
    t14 = (t19 + 4);
    t26 = (t15 + 4);
    t20 = *((unsigned int *)t15);
    t21 = (t20 >> 0);
    *((unsigned int *)t19) = t21;
    t22 = *((unsigned int *)t26);
    t23 = (t22 >> 0);
    *((unsigned int *)t14) = t23;
    t24 = *((unsigned int *)t19);
    *((unsigned int *)t19) = (t24 & 3U);
    t25 = *((unsigned int *)t14);
    *((unsigned int *)t14) = (t25 & 3U);
    xsi_vlogtype_concat(t16, 8, 8, 2U, t19, 2, t17, 6);
    t27 = (t0 + 1928);
    xsi_vlogvar_assign_value(t27, t16, 0, 0, 8);
    goto LAB46;

}


extern void work_m_00000000003893690840_2480586375_init()
{
	static char *pe[] = {(void *)Always_34_0};
	xsi_register_didat("work_m_00000000003893690840_2480586375", "isim/multi_funct_barrel_shifter_tb_isim_beh.exe.sim/work/m_00000000003893690840_2480586375.didat");
	xsi_register_executes(pe);
}
