`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   15:38:03 09/20/2018
// Design Name:   multi_func_barrel_shifter
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.1/multi_funct_barrel_shifter_tb.v
// Project Name:  example3.11.1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: multi_func_barrel_shifter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module multi_funct_barrel_shifter_tb;

	// Inputs
	reg [7:0] test_in;
	reg test_lr;
	reg [2:0] test_amt;

	// Outputs
	wire [7:0] test_out;

	// Instantiate the Unit Under Test (UUT)
	multi_func_barrel_shifter uut (.in(test_in), .lr(test_lr), .amt(test_amt), .out(test_out));

	initial begin
		// Initialize Inputs
		test_in = 5;
		test_lr = 0;
		test_amt = 3;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		test_in = 11;
		test_lr = 1;
		test_amt = 6;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_in = 13;
		test_lr = 0;
		test_amt = 2;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_in = 87;
		test_lr = 0;
		test_amt = 5;

		// Wait 100 ns for global reset to finish
		#100;
		
		test_in = 54;
		test_lr = 1;
		test_amt = 1;

		// Wait 100 ns for global reset to finish
		#100;
		$stop;

	end
      
endmodule

