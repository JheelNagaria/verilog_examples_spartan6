`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:49:58 09/20/2018 
// Design Name: 
// Module Name:    multi_func_barrel_shifter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module multi_func_barrel_shifter(
    input wire [7:0] in,
    input wire lr,
	 input wire [2:0] amt,
    output reg [7:0] out
    );
	 
	 reg [7:0] right_buffer, left_buffer;
	 wire buff_en;
	 
	 rotate_right rot_right(.in(in), .out(right_buffer));
	 rotate_left rot_left(.in(in), .out(left_buffer));
	 	 
	 always @(in, lr, amt)
	 begin
	 
		if (lr)
		begin
			
			case (amt)
				3'o0: out = in;
				3'o1: out = {in[0], in[7:1]};
				3'o2: out = {in[1:0], in[7:2]};
				3'o3: out = {in[2:0], in[7:3]};
				3'o4: out = {in[3:0], in[7:4]};
				3'o5: out = {in[4:0], in[7:5]};
				3'o6: out = {in[5:0], in[7:6]};
				default : out = {in[6:0], in[7]};
			endcase
		
		end
		
		else 
		begin
			
			case (amt)
				3'o0: out = in;
				3'o1: out = {in[6:0], in[7]};
				3'o2: out = {in[5:0], in[7:6]};
				3'o3: out = {in[4:0], in[7:5]};
				3'o4: out = {in[3:0], in[7:4]};
				3'o5: out = {in[2:0], in[7:3]};
				3'o6: out = {in[1:0], in[7:2]};
				default : out = {in[0], in[7:1]};
			endcase
		
		end
		
	 end
endmodule
