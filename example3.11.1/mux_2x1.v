`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:50:46 09/20/2018 
// Design Name: 
// Module Name:    mux_2x1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mux_2x1(
    input wire in_0,
    input wire in_1,
    input wire sel,
    output reg out
    );
	 
	 always @*
	 begin
		
		if (~sel)
			out <= in_0;
		else out <= in_1;
	
	end

endmodule
