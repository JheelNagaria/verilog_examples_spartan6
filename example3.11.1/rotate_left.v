`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:57:25 09/20/2018 
// Design Name: 
// Module Name:    rotate_left 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rotate_left(
    input wire [7:0] in,
    output reg [7:0] out
    );
	 
	 always @(in)
	 begin
		out = {in[6:0], in[7]};
	 end

endmodule
