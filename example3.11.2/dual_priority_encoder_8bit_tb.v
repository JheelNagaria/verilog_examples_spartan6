`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:13:05 09/22/2018
// Design Name:   dual_priority_encoder_8bit
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.2/dual_priority_encoder_8bit_tb.v
// Project Name:  example3.11.2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: dual_priority_encoder_8bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module dual_priority_encoder_8bit_tb;

	// Inputs
	reg [7:0] test_a;
	reg test_clk;

	// Outputs
	wire [2:0] test_d1;
	wire [2:0] test_d2;
	wire test_seg_7_1_en, test_seg_7_2_en, test_seg_7_3_en, test_seg_7_4_en;
	wire [6:0] test_seg_7_1;

	// Instantiate the Unit Under Test (UUT)
	dual_priority_encoder_8bit uut (.clk_100MHz(test_clk), .a(test_a), .d1(test_d1),	.d2(test_d2), .seg_7_1_en(test_seg_7_1_en),
	.seg_7_2_en(test_seg_7_2_en), .seg_7_3_en(test_seg_7_3_en), .seg_7_4_en(test_seg_7_4_en),
	.seg_7_1(test_seg_7_1)
	);
	
	initial test_clk = 0;
	
	always #10 test_clk = ~test_clk;

	initial begin
		// Initialize Inputs
		test_a = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		for (test_a = 1; test_a < 256; test_a = test_a + 1) begin
			#100;
		end

	end
      
endmodule

