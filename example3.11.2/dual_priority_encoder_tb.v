`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:22:06 09/20/2018
// Design Name:   dual_priority_encoder
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.2/dual_priority_encoder_tb.v
// Project Name:  example3.11.2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: dual_priority_encoder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module dual_priority_encoder_tb;

	// Inputs
	reg [3:0] test_a;

	// Outputs
	wire [1:0] test_d1;
	wire [1:0] test_d2;
	
	// Loop variables
	integer i = 0;

	// Instantiate the Unit Under Test (UUT)
	dual_priority_encoder uut (.a(test_a), .d1(test_d1), .d2(test_d2));

	initial begin
		// Initialize Inputs
		test_a = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		for (i = 1; i < 16; i = i + 1) begin
			test_a = i;
			
			#100;
      end
		
		$stop;
		// Add stimulus here

	end
      
endmodule

