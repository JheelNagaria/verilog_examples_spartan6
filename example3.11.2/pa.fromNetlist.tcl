
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name example3.11.2 -dir "E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.2/planAhead_run_1" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.2/dual_priority_encoder_8bit.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.2} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "dual_priority_encoder.ucf" [current_fileset -constrset]
add_files [list {dual_priority_encoder.ucf}] -fileset [get_property constrset [current_run]]
link_design
