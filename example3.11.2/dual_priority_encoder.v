`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:23:35 09/20/2018 
// Design Name: 
// Module Name:    dual_priority_encoder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dual_priority_encoder(
    input wire [3:0] a,
    output reg [1:0] d1,
    output reg [1:0] d2
    );
	 
	 always @(a)
	 begin
	 
		case (a)
			0 : begin
				d1 = 2'bxx;
				d2 = 2'bxx;
			end
			
			1 : begin
				d1 = 2'b00;
				d2 = 2'bxx;
			end
			
			2 : begin
				d1 = 2'b01;
				d2 = 2'bxx;
			end
			
			4 : begin
				d1 = 2'b10;
				d2 = 2'bxx;
			end
			
			8 : begin
				d1 = 2'b11;
				d2 = 2'bxx;
			end
		
			default: begin
				d1[0] = a[3] | ((~a[2]) & a[1]);
				d1[1] = a[2] | a[3];
		
				d2[0] = a[1] & (a[3] ^ a[2]);
				d2[1] = a[3] & a[2];
			end
		endcase
		
		
	 
	 end
endmodule
