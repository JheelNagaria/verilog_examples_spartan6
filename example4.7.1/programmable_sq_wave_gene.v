`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:53:17 10/10/2018 
// Design Name: 
// Module Name:    programmable_sq_wave_gene 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module programmable_sq_wave_gene(
	 input wire clk_50MHz,
    input wire [7:0] in,
    output reg out
    );
	 
	 reg [6:0] count		= 0; 
	 wire [6:0] count_m 	= in[7:4] * 3'b101;
	 wire [6:0] count_n 	= in[3:0] * 3'b101;
	 
	 
	 always @(posedge clk_50MHz) begin
			count = count + 1;
			
			if (count < count_m)
				out <= 1'b1;
			else if (count < count_m + count_n)
				out <= 1'b0;
			else count = 0 ;
			end

endmodule
