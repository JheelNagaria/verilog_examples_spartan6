
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name example4.7.1 -dir "E:/Jheel/PongPChuExamples/example4.7.1/planAhead_run_2" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/PongPChuExamples/example4.7.1/programmable_sq_wave_gene.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/PongPChuExamples/example4.7.1} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "programmable_sq_wave_gene.ucf" [current_fileset -constrset]
add_files [list {programmable_sq_wave_gene.ucf}] -fileset [get_property constrset [current_run]]
link_design
