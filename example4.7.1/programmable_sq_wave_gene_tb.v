`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   14:50:25 10/10/2018
// Design Name:   programmable_sq_wave_gene
// Module Name:   E:/Jheel/PongPChuExamples/example4.7.1/programmable_sq_wave_gene_tb.v
// Project Name:  example4.7.1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: programmable_sq_wave_gene
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module programmable_sq_wave_gene_tb;

	// Inputs
	reg clk_50MHz;
	reg [7:0] in;

	// Outputs
	wire out;
	
	// Loop Variables
	//integer i = 0;

	// Instantiate the Unit Under Test (UUT)
	programmable_sq_wave_gene uut (
		.clk_50MHz(clk_50MHz), 
		.in(in), 
		.out(out)
	);

	initial begin
		// Initialize Inputs
		clk_50MHz = 0;
		in = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		in = 52;
		#2000;

	end
	
	always #20 clk_50MHz = !clk_50MHz;
      
endmodule

