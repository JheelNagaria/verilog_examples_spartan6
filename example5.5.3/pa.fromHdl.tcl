
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name example5.5.3 -dir "E:/Jheel/verilog_examples_spartan6/example5.5.3/planAhead_run_1" -part xc6slx16csg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "parkinglot_occ_counter.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {parkinglot_occ_counter.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top parkinglot_occ_counter $srcset
add_files [list {parkinglot_occ_counter.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx16csg324-3
