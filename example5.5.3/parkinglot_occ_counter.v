`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:25:19 10/18/2018 
// Design Name: 
// Module Name:    parkinglot_occ_counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module parkinglot_occ_counter(
	 input wire clk,
    input wire sens_1,
    input wire sens_2,
    output reg [3:0] an,
    output wire [6:0] cath
    );
	 
	 reg [6:0] dig;
	 reg [19:0] refresh_counter;
	 
	 reg [15:0] car_count = 0;
	 reg [3:0] bcd_car_count [3:0];
	 
	 localparam s0 = 2'b00,
					s1 = 2'b01,
					s2 = 2'b10,
					s3 = 2'b11;
	 
	 reg [1:0] state_reg, state_next;
	 
	 reg enter, exit, inc;
	 
	 integer i = 0;
	 
	 
	 bcd_to_sseg bcd_2_ssg(.bcd(dig), .sseg(cath));
	 
	 
	 always @(posedge clk)
		refresh_counter = refresh_counter + 1;
		
	 
	 always @(posedge clk)
		state_reg <= state_next;
		
	 
	 always @(*)
	 begin
	 
		state_next = state_reg;
		enter = 1'b0;
		exit = 1'b0;
		inc = 1'b0;
		
		case (state_reg)
		
			s0: begin
				if (sens_1 == 1)
				begin
					enter = 1'b1;
					exit = 1'b0;
					inc = 1'b0;
					state_next = s1;
				end
				
				else state_next = s0;
			end
			
			
			s1: begin
				if (sens_1 == 1 && sens_2 == 1)
				begin
					enter = 1'b0;
					inc = 1'b1;
					exit = 1'b0;
					state_next = s2;
				end
				
				else if (sens_1 == 0 && sens_2 == 0)
				begin
					inc = 1'b0;
					enter = 1'b0;
					exit = 1'b0;
					state_next = s0;
				end
			end
			
			s2: begin
				if (sens_1 == 1 && sens_2 == 1)
				begin
					enter = 1'b0;
					inc = 1'b0;
					exit = 1'b0;
					state_next = s2;
				end
				
				else if (sens_1 == 0 && sens_2 == 1)
				begin
					inc = 1'b0;
					enter = 1'b0;
					exit = 1'b0;
					state_next = s3;
				end
			end
			
			s3: begin
				if (sens_1 == 0 && sens_2 == 0)
				begin
					enter = 1'b0;
					inc = 1'b0;
					exit = 1'b1;
					state_next = s0;
				end
			end
		
		endcase
	 
	 end
	
	 
	 always @(posedge inc)
		car_count = (car_count == 9999) ? (0) : (car_count + 1);
		
	 always @(car_count)
	 begin
	 
	 bcd_car_count[0] = 4'b0000;
	 bcd_car_count[1] = 4'b0000;
	 bcd_car_count[2] = 4'b0000;
	 bcd_car_count[3] = 4'b0000;
	 
	 
	 for (i = 15; i >= 0; i = i - 1)
		begin
		
			if (bcd_car_count[3] >= 4'b0101)
				bcd_car_count[3] = bcd_car_count[3] + 4'b0011;
			
			if (bcd_car_count[2] >= 4'b0101)
				bcd_car_count[2] = bcd_car_count[2] + 4'b0011;
			
			if (bcd_car_count[1] >= 4'b0101)
				bcd_car_count[1] = bcd_car_count[1] + 4'b0011;
			
			if (bcd_car_count[0] >= 4'b0101)
				bcd_car_count[0] = bcd_car_count[0] + 4'b0011;
			
			
			bcd_car_count[3] = bcd_car_count[3] << 1;
			bcd_car_count[3][0] = bcd_car_count[2][3];
			
			bcd_car_count[2] = bcd_car_count[2] << 1;
			bcd_car_count[2][0] = bcd_car_count[1][3];
			
			bcd_car_count[1] = bcd_car_count[1] << 1;
			bcd_car_count[1][0] = bcd_car_count[0][3];
			
			bcd_car_count[0] = bcd_car_count[0] << 1;
			bcd_car_count[0][0] = car_count[i];
		
		end
	 
	 end
	 
	 
	 always @(refresh_counter)
	 begin
	 
		case (refresh_counter[19:18])
			
			2'b00 : begin
				dig = bcd_car_count[0];
				an = 4'b1110;
			end
			
			2'b01 : begin
				dig = bcd_car_count[1];
				an = 4'b1101;
			end
			
			2'b10 : begin
				dig = bcd_car_count[2];
				an = 4'b1011;
			end
			
			2'b11 : begin
				dig = bcd_car_count[3];
				an = 4'b0111;
			end
			
		endcase
	 end

endmodule


module bcd_to_sseg(
	 input [3:0] bcd,
	 output reg [6:0] sseg
	 );
	 
	 always @(bcd)
	 begin
		
		case (bcd)
			
			4'b0000 : sseg <= 7'b0000001;
			4'b0001 : sseg <= 7'b1001111;
			4'b0010 : sseg <= 7'b0010010;
			4'b0011 : sseg <= 7'b0000110;
			4'b0100 : sseg <= 7'b1001100;
			4'b0101 : sseg <= 7'b0100100;
			4'b0110 : sseg <= 7'b0100000;
			4'b0111 : sseg <= 7'b0001111;
			4'b1000 : sseg <= 7'b0000000;
			4'b1001 : sseg <= 7'b0000100;
			default : sseg <= 7'b1111111;
			
		endcase
		
	 end
	 
endmodule