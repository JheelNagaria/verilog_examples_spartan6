
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name example4.7.4 -dir "E:/Jheel/verilog_examples_spartan6/example4.7.4/planAhead_run_1" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/verilog_examples_spartan6/example4.7.4/heartbeat.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/verilog_examples_spartan6/example4.7.4} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "heartbeat.ucf" [current_fileset -constrset]
add_files [list {heartbeat.ucf}] -fileset [get_property constrset [current_run]]
link_design
