`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:21:17 10/12/2018 
// Design Name: 
// Module Name:    heartbeat 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module heartbeat(
    input clk,
    output reg [3:0] an,
    output reg [6:0] cath
    );
	 
	 
	 reg [26:0] counter = 0;
	 wire signal_72hz;
	 reg [19:0] refresh_counter;
	 reg [1:0] stage;
	 
	 
	 always @(posedge clk)
		counter = counter + 1;
		
	 
	 assign signal_72hz = (counter == 1388889) ? 1'b1 : 1'b0;
	 
	 
	 always @(posedge clk)
		refresh_counter = refresh_counter + 1;
		
	 
	 always @(posedge signal_72hz)
		stage = stage + 1;
	 
	 
	 always @(stage)
	 begin
	 
		case (stage)
			
			2'b00: begin
				
				case (refresh_counter[19:18])
					
					2'b00: begin
						an <= 4'b1110;
						cath <= 7'b1111111;						
					end
					
					2'b01: begin
						an <= 4'b1101;
						cath <= 7'b1111001;						
					end
					
					2'b10: begin
						an <= 4'b1011;
						cath <= 7'b1001111;						
					end
					
					2'b11: begin
						an <= 4'b0111;
						cath <= 7'b1111111;						
					end
				
				endcase
			end
			
			2'b01: begin
				
				case (refresh_counter[19:18])
					
					2'b00: begin
						an <= 4'b1110;
						cath <= 7'b1111111;						
					end
					
					2'b01: begin
						an <= 4'b1101;
						cath <= 7'b1001111;						
					end
					
					2'b10: begin
						an <= 4'b1011;
						cath <= 7'b1111001;						
					end
					
					2'b11: begin
						an <= 4'b0111;
						cath <= 7'b1111111;						
					end
				
				endcase
			end
			
			2'b10: begin
				
				case (refresh_counter[19:18])
					
					2'b00: begin
						an <= 4'b1110;
						cath <= 7'b1001111;						
					end
					
					2'b01: begin
						an <= 4'b1101;
						cath <= 7'b1111111;						
					end
					
					2'b10: begin
						an <= 4'b1011;
						cath <= 7'b1111111;						
					end
					
					2'b11: begin
						an <= 4'b0111;
						cath <= 7'b1111001;						
					end
				
				endcase
			end
			
			2'b00:
				an <= 4'b1111;
				
		endcase
	 
	 end


endmodule
