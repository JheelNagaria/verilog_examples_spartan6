`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:39:10 10/18/2018 
// Design Name: 
// Module Name:    enhanced_stop_watch 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module enhanced_stop_watch(
    input clk,
    output reg [3:0] an,
    output wire [6:0] cath,
	 output reg dp
    );
	 
	 reg [23:0] D_counter = 0;
	 reg [26:0] SS_counter = 0;
	 
	 wire D_signal, SS_signal, M_signal;
	 
	 reg [3:0] D;
	 reg [5:0] SS;
	 reg [3:0] M;
	  
	 reg [3:0] dig;
	 reg [3:0] bcd_SS [1:0];
	 
	 reg [19:0] refresh_counter = 0;
	 
	 integer i, j;
	 
	 bcd_to_sseg bcd_2_ssg(.bcd(dig), .sseg(cath));
	 
	 always @(posedge clk)
	 begin
			
		D_counter = (D_counter == 10000000) ? (0) : (D_counter + 1);
		SS_counter = (SS_counter == 100000000) ? (0) : (SS_counter + 1);
		refresh_counter = refresh_counter + 1;
		
	 end
	 
	 assign D_signal = (D_counter == 10000000) ? (1'b1) : (1'b0);
	 assign SS_signal = (SS_counter == 100000000) ? (1'b1) : (1'b0);
	 
	 always @(posedge D_signal)
		D = (D == 4'b1001) ? (4'b0000) : (D + 4'b0001);
		
	 always @(posedge SS_signal)
		SS = (SS == 6'b111011) ? (6'b000000) : (SS + 6'b000001);
		
	 assign M_signal = (SS == 6'b111011) ? (1'b1) : (1'b0);
	 
	 always @(negedge M_signal)
		M = (M == 4'b1001) ? (4'b0000) : (M + 4'b0001);
		
	 always @(SS)
	 begin
	 
		bcd_SS[1] = 4'b0000;
		bcd_SS[0] = 4'b0000;
		
		for (i = 5; i >= 0; i = i - 1)
		begin
		
			if (bcd_SS[1] >= 4'b0101)
				bcd_SS[1] = bcd_SS[1] + 4'b0011;
				
			if (bcd_SS[0] >= 4'b0101)
				bcd_SS[0] = bcd_SS[0] + 4'b0011;
				
			bcd_SS[1] = bcd_SS[1] << 1;
			bcd_SS[1][0] = bcd_SS[0][3];
			
			bcd_SS[0] = bcd_SS[0] << 1;
			bcd_SS[0][0] = SS[i];
		
		end
	 
	 end
	 
	 always @(refresh_counter[19:18])
	 begin
	 
		case (refresh_counter[19:18])
			
			2'b00 : begin
				dig = M;
				an = 4'b0111;
				dp = 1'b0;
			end
			
			2'b01 : begin
				dig = bcd_SS[1];
				an = 4'b1011;
				dp = 1'b1;
			end
			
			2'b10 : begin
				dig = bcd_SS[0];
				an = 4'b1101;
				dp = 1'b0;
			end
			
			2'b11 : begin
				dig = D;
				an = 4'b1110;
				dp = 1'b1;
			end
			
		endcase
	 
	 end


endmodule


module bcd_to_sseg(
	 input [3:0] bcd,
	 output reg [6:0] sseg
	 );
	 
	 always @(bcd)
	 begin
		
		case (bcd)
			
			4'b0000 : sseg <= 7'b0000001;
			4'b0001 : sseg <= 7'b1001111;
			4'b0010 : sseg <= 7'b0010010;
			4'b0011 : sseg <= 7'b0000110;
			4'b0100 : sseg <= 7'b1001100;
			4'b0101 : sseg <= 7'b0100100;
			4'b0110 : sseg <= 7'b0100000;
			4'b0111 : sseg <= 7'b0001111;
			4'b1000 : sseg <= 7'b0000000;
			4'b1001 : sseg <= 7'b0000100;
			default : sseg <= 7'b1111111;
			
		endcase
		
	 end
	 
endmodule
