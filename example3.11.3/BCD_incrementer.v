`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:16:07 10/05/2018 
// Design Name: 
// Module Name:    BCD_incrementer 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////


module BCD_incrementer(
    input clock_100Mhz, 
    input reset,
    output reg [3:0] Anode_Activate,
    output reg [6:0] LED_out
    );
    
	 
	 
	 reg [26:0] one_second_counter;
    wire one_second_enable;
    reg [3:0] digit_1 = 0, digit_2 = 0, digit_3 = 0; 
	 reg [3:0] prev_dig_1 = 0, prev_dig_2 = 0, prev_dig_3 = 0;
    reg [3:0] LED_BCD;
    reg [19:0] refresh_counter; 
    wire [1:0] LED_activating_counter; 
    
	 
	 always @(posedge clock_100Mhz or posedge reset)
    begin
        if(reset==1)
            one_second_counter <= 0;
        else begin
            if(one_second_counter >= 10000000) 
                 one_second_counter <= 0;
            else
                one_second_counter <= one_second_counter + 1;
        end
    end 
  	 
	 assign one_second_enable = (one_second_counter == 10000000)?1:0;
    
	 always @(posedge one_second_enable)
    begin
        if(reset==1) begin
            digit_1 <= 0;
				digit_2 <= 0;
				digit_3 <= 0;
		  end
		  
		  else begin
		  
				prev_dig_1 <= digit_1;
				prev_dig_2 <= digit_2;
				prev_dig_3 <= digit_3;
		  
				digit_1[0] <= ~prev_dig_1[0];
				digit_1[1] <= ((~prev_dig_1[3]) & (~prev_dig_1[1]) & prev_dig_1[0]) | (prev_dig_1[1] & (~prev_dig_1[0]));
				digit_1[2] <= ((~prev_dig_1[2]) & (prev_dig_1[1]) & (prev_dig_1[0])) | (prev_dig_1[2] & ((~prev_dig_1[1]) | (~prev_dig_1[0])));
				digit_1[3] <= (prev_dig_1[2] & prev_dig_1[1] & prev_dig_1[0]) | (prev_dig_1[3] & (~prev_dig_1[0]));
				
				if (prev_dig_1 == 4'b1001) begin
						digit_2[0] <= ~prev_dig_2[0];
						digit_2[1] <= ((~prev_dig_2[3]) & (~prev_dig_2[1]) & prev_dig_2[0]) | (prev_dig_2[1] & (~prev_dig_2[0]));
						digit_2[2] <= ((~prev_dig_2[2]) & (prev_dig_2[1]) & (prev_dig_2[0])) | (prev_dig_2[2] & ((~prev_dig_2[1]) | (~prev_dig_2[0])));
						digit_2[3] <= (prev_dig_2[2] & prev_dig_2[1] & prev_dig_2[0]) | (prev_dig_2[3] & (~prev_dig_2[0]));
				end
			
				if (prev_dig_2 == 4'b1001 && prev_dig_1 == 4'b1001) begin
						digit_3[0] <= ~prev_dig_3[0];
						digit_3[1] <= ((~prev_dig_3[3]) & (~prev_dig_3[1]) & prev_dig_3[0]) | (prev_dig_3[1] & (~prev_dig_3[0]));
						digit_3[2] <= ((~prev_dig_3[2]) & (prev_dig_3[1]) & (prev_dig_3[0])) | (prev_dig_3[2] & ((~prev_dig_3[1]) | (~prev_dig_3[0])));
						digit_3[3] <= (prev_dig_3[2] & prev_dig_3[1] & prev_dig_3[0]) | (prev_dig_3[3] & (~prev_dig_3[0]));
				end
		  end
    end
    
	 
	 always @(posedge clock_100Mhz or posedge reset)
    begin 
        if(reset==1)
            refresh_counter <= 0;
        else
            refresh_counter <= refresh_counter + 1;
    end 
    
	 
	 assign LED_activating_counter = refresh_counter[19:18];
	 
	 always @(*)
    begin
        case(LED_activating_counter)
        2'b00: begin
            Anode_Activate = 4'b1110; 
            LED_BCD = digit_1;
              end
       
		  2'b01: begin
            Anode_Activate = 4'b1101; 
            LED_BCD = digit_2;
              end
        
		  2'b10: begin
            Anode_Activate = 4'b1011; 
            LED_BCD = digit_3;
                end
        
		  2'b11: begin
            Anode_Activate = 4'b0111; 
            LED_BCD = 4'b0000;
               end
        
		  endcase
    end
    
	 
	 
	 // Cathode patterns of the 7-segment LED display 
    always @(LED_BCD)
    begin
        case(LED_BCD)
        4'b0000: LED_out = 7'b0000001; // "0"     
        4'b0001: LED_out = 7'b1001111; // "1" 
        4'b0010: LED_out = 7'b0010010; // "2" 
        4'b0011: LED_out = 7'b0000110; // "3" 
        4'b0100: LED_out = 7'b1001100; // "4" 
        4'b0101: LED_out = 7'b0100100; // "5" 
        4'b0110: LED_out = 7'b0100000; // "6" 
        4'b0111: LED_out = 7'b0001111; // "7" 
        4'b1000: LED_out = 7'b0000000; // "8"     
        4'b1001: LED_out = 7'b0000100; // "9" 
        default: LED_out = 7'b0000001; // "0"
        endcase
	 end
endmodule

