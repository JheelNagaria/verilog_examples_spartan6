`timescale 1 ns / 1 ns

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:54:09 10/05/2018
// Design Name:   BCD_incrementer
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example3.11.3/BCD_incrementer_tb.v
// Project Name:  example3.11.3
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: BCD_incrementer
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module BCD_incrementer_tb;

	// Inputs
	reg clock_100Mhz;
	reg reset;

	// Outputs
	wire [3:0] Anode_Activate;
	wire [6:0] LED_out;

	// Instantiate the Unit Under Test (UUT)
	BCD_incrementer uut (
		.clock_100Mhz(clock_100Mhz), 
		.reset(reset), 
		.Anode_Activate(Anode_Activate), 
		.LED_out(LED_out)
	);
	
	always 
		#10 clock_100Mhz = !clock_100Mhz;

	initial begin
		// Initialize Inputs
		clock_100Mhz = 0;
		reset = 0;

		// Wait 100 ns for global reset to finish
		#1000;
        
		// Add stimulus here
	end
	
	

     
endmodule

