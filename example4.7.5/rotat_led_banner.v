`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:27:19 10/16/2018 
// Design Name: 
// Module Name:    rotat_led_banner 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rotat_led_banner(
    input clk,
    input en,
    input dir,
    output reg [3:0] an,
    output wire [6:0] cath
    );
	 
	 reg [26:0] counter = 0;
	 wire one_second_clk;
	
 	 reg [31:0] binary = 32'd3967116328;
	 integer i;
	 integer j;
	 reg [3:0] bcd_digit [9:0];
	
	 reg [19:0] refresh_counter;
	 
	 integer stage = 12;
	 reg [3:0] dig;
	 
	 bcd_to_sseg bcd_2_ssg(.bcd(dig), .sseg(cath));
	 
	 
	 always @(posedge clk)
	 begin	 
		
		counter = (counter == 100000000) ? (0) : (counter + 1);
		refresh_counter = refresh_counter + 1;
		
	 end
	 
	 
	 assign one_second_clk = (counter == 100000000) ? (1'b1) : (1'b0);
	 
	 always @(binary)
	 begin
	 
	 bcd_digit[9] = 4'b0000;
	 bcd_digit[8] = 4'b0000;
	 bcd_digit[7] = 4'b0000;
	 bcd_digit[6] = 4'b0000;
	 bcd_digit[5] = 4'b0000;
	 bcd_digit[4] = 4'b0000;
	 bcd_digit[3] = 4'b0000;
	 bcd_digit[2] = 4'b0000;
	 bcd_digit[1] = 4'b0000;
	 bcd_digit[0] = 4'b0000;
	 
		for (i = 31; i >= 0; i = i - 1)
		begin
		
			if (bcd_digit[9] >= 4'b0101)
				bcd_digit[9] = bcd_digit[9] + 4'b0011;
				
			if (bcd_digit[8] >= 4'b0101)
				bcd_digit[8] = bcd_digit[8] + 4'b0011;
			
			if (bcd_digit[7] >= 4'b0101)
				bcd_digit[7] = bcd_digit[7] + 4'b0011;
			
			if (bcd_digit[6] >= 4'b0101)
				bcd_digit[6] = bcd_digit[6] + 4'b0011;
			
			if (bcd_digit[5] >= 4'b0101)
				bcd_digit[5] = bcd_digit[5] + 4'b0011;
			
			if (bcd_digit[4] >= 4'b0101)
				bcd_digit[4] = bcd_digit[4] + 4'b0011;
			
			if (bcd_digit[3] >= 4'b0101)
				bcd_digit[3] = bcd_digit[3] + 4'b0011;
			
			if (bcd_digit[2] >= 4'b0101)
				bcd_digit[2] = bcd_digit[2] + 4'b0011;
			
			if (bcd_digit[1] >= 4'b0101)
				bcd_digit[1] = bcd_digit[1] + 4'b0011;
			
			if (bcd_digit[0] >= 4'b0101)
				bcd_digit[0] = bcd_digit[0] + 4'b0011;
				
				
			bcd_digit[9] = bcd_digit[9] << 1;
			bcd_digit[9][0] = bcd_digit[8][3];
			
			bcd_digit[8] = bcd_digit[8] << 1;
			bcd_digit[8][0] = bcd_digit[7][3];
			
			bcd_digit[7] = bcd_digit[7] << 1;
			bcd_digit[7][0] = bcd_digit[6][3];
			
			bcd_digit[6] = bcd_digit[6] << 1;
			bcd_digit[6][0] = bcd_digit[5][3];
			
			bcd_digit[5] = bcd_digit[5] << 1;
			bcd_digit[5][0] = bcd_digit[4][3];
			
			bcd_digit[4] = bcd_digit[4] << 1;
			bcd_digit[4][0] = bcd_digit[3][3];
			
			bcd_digit[3] = bcd_digit[3] << 1;
			bcd_digit[3][0] = bcd_digit[2][3];
			
			bcd_digit[2] = bcd_digit[2] << 1;
			bcd_digit[2][0] = bcd_digit[1][3];
			
			bcd_digit[1] = bcd_digit[1] << 1;
			bcd_digit[1][0] = bcd_digit[0][3];
			
			bcd_digit[0] = bcd_digit[0] << 1;
			bcd_digit[0][0] = binary[i];
		
		end
	 
	 end
	 
	 always @(posedge one_second_clk)
	 begin
		if (en == 1'b1)
			if (dir == 1'b1)
				stage = (stage == 0) ? (12) : (stage - 1);
			else
				stage = (stage == 12) ? 0 : (stage + 1);
			
	 end
	
	 
	 always @(refresh_counter)
	 begin
	 
		case (refresh_counter[19:18])
			
			2'b00 : begin
				dig = ((stage - 3) >= 0) ? (bcd_digit[stage - 3]) : (4'b1010);
				an = ((stage - 3) >= 0) ? (4'b0111) : (4'b1111);
			end
			
			2'b01 : begin
				dig = ((stage - 4) >= 0) ? (bcd_digit[stage - 4]) : (4'b1010);
				an = ((stage - 4) >= 0) ? (4'b1011) : (4'b1111);
			end
			
			2'b10 : begin
				dig = ((stage - 5) >= 0) ? (bcd_digit[stage - 5]) : (4'b1010);
				an = ((stage - 5) >= 0) ? (4'b1101) : (4'b1111);
			end
			
			2'b11 : begin
				dig = ((stage - 6) >= 0) ? (bcd_digit[stage - 6]) : (4'b1010);
				an = ((stage - 6) >= 0) ? (4'b1110) : (4'b1111);
			end
			
		endcase
		
	 end

endmodule




module bcd_to_sseg(
	 input [3:0] bcd,
	 output reg [6:0] sseg
	 );
	 
	 always @(bcd)
	 begin
		
		case (bcd)
			
			4'b0000 : sseg <= 7'b0000001;
			4'b0001 : sseg <= 7'b1001111;
			4'b0010 : sseg <= 7'b0010010;
			4'b0011 : sseg <= 7'b0000110;
			4'b0100 : sseg <= 7'b1001100;
			4'b0101 : sseg <= 7'b0100100;
			4'b0110 : sseg <= 7'b0100000;
			4'b0111 : sseg <= 7'b0001111;
			4'b1000 : sseg <= 7'b0000000;
			4'b1001 : sseg <= 7'b0000100;
			default : sseg <= 7'b1111111;
			
		endcase
		
	 end
	 
endmodule