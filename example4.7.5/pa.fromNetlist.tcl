
# PlanAhead Launch Script for Post-Synthesis floorplanning, created by Project Navigator

create_project -name example4.7.5 -dir "E:/Jheel/verilog_examples_spartan6/example4.7.5/planAhead_run_2" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Jheel/verilog_examples_spartan6/example4.7.5/rotat_led_banner.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Jheel/verilog_examples_spartan6/example4.7.5} }
set_property target_constrs_file "rotat_led_banner.ucf" [current_fileset -constrset]
add_files [list {rotat_led_banner.ucf}] -fileset [get_property constrset [current_run]]
link_design
