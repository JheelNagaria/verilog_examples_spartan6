`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:01:21 09/19/2018 
// Design Name: 
// Module Name:    decode_2x4 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module decoder_2x4(
    input wire en,
    input wire [1:0] in,
    output wire [3:0] d
    );
	 
	 assign d[0] = en & ~in[1] & ~in[0];
	 assign d[1] = en & ~in[1] & in[0];
	 assign d[2] = en & in[1] & ~in[0];
	 assign d[3] = en & in[1] & in[0];

endmodule
