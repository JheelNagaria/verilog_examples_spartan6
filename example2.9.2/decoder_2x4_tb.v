`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:03:33 09/19/2018
// Design Name:   decode_2x4
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.2/decoder_2x4_tb.v
// Project Name:  example2.9.2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: decode_2x4
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module decoder_2x4_tb;

	// Inputs
	reg test_en;
	reg [1:0] test_in;

	// Outputs
	wire [3:0] test_d;

	// Loop variables
	integer i = 0;
	
	// Instantiate the Unit Under Test (UUT)
	decoder_2x4 uut (.en(test_en), .in(test_in), .d(test_d));

	initial begin
		// Initialize Inputs
		test_en = 0;
		test_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Add stimulus here
		for (i = 0 ; i < 8; i = i + 1) begin
			test_en = i[2];
			test_in = {i[1], i[0]};
			
			#100;
      end
		$stop;

	end
      
endmodule

