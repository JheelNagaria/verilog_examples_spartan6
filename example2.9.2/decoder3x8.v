`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:34:33 09/19/2018 
// Design Name: 
// Module Name:    decoder3x8 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module decoder_3x8(
    input wire [2:0] in,
    output wire [7:0] d
    );
	 
	 decoder_2x4 d7tod4(.en(in[2]), .in({in[1], in[0]}), .d({d[7], d[6], d[5], d[4]}));
	 decoder_2x4 d3tod0(.en(~in[2]), .in({in[1], in[0]}), .d({d[3], d[2], d[1], d[0]}));

endmodule
