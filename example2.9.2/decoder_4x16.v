`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:54:37 09/19/2018 
// Design Name: 
// Module Name:    decoder_4x16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module decoder_4x16(
    input wire [3:0] in,
    output wire [15:0] d
    );
	 
	 wire en_d15tod12, en_d11tod8, en_d7tod4, en_d3tod0;
	 
	 assign en_d3tod0 = ~in[3] & ~in[2];
	 assign en_d7tod4 = ~in[3] & in[2];
	 assign en_d11tod8 = in[3] & ~in[2];
	 assign en_d15tod12 = in[3] & in[2];
	 
	 decoder_2x4 d15tod12(.en(en_d15tod12), .in({in[1], in[0]}), .d({d[15], d[14], d[13], d[12]}));
	 decoder_2x4 d11tod8(.en(en_d11tod8), .in({in[1], in[0]}), .d({d[11], d[10], d[9], d[8]}));
	 decoder_2x4 d7tod4(.en(en_d7tod4), .in({in[1], in[0]}), .d({d[7], d[6], d[5], d[4]}));
	 decoder_2x4 d3tod0(.en(en_d3tod0), .in({in[1], in[0]}), .d({d[3], d[2], d[1], d[0]}));


endmodule
