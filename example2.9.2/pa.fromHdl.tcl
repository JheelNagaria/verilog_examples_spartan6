
# PlanAhead Launch Script for Pre-Synthesis Floorplanning, created by Project Navigator

create_project -name example2.9.2 -dir "E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.2/planAhead_run_2" -part xc6slx16csg324-3
set_param project.pinAheadLayout yes
set srcset [get_property srcset [current_run -impl]]
set_property target_constrs_file "decode_2x4.ucf" [current_fileset -constrset]
set hdlfile [add_files [list {decode_2x4.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set hdlfile [add_files [list {decoder3x8.v}]]
set_property file_type Verilog $hdlfile
set_property library work $hdlfile
set_property top decoder_3x8 $srcset
add_files [list {decode_2x4.ucf}] -fileset [get_property constrset [current_run]]
open_rtl_design -part xc6slx16csg324-3
