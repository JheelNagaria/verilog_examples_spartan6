`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   13:40:49 09/19/2018
// Design Name:   decoder3x8
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.2/decoder_3x8_tb.v
// Project Name:  example2.9.2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: decoder3x8
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module decoder_3x8_tb;

	// Inputs
	reg [2:0] test_in;

	// Outputs
	wire [7:0] test_d;

	// Loop variables
	integer i = 0;

	// Instantiate the Unit Under Test (UUT)
	decoder_3x8 uut (.in(test_in), .d(test_d));

	initial begin
		// Initialize Inputs

		// Wait 100 ns for global reset to finish
        
		// Add stimulus here
		
		for (i = 0; i < 8; i = i + 1) begin
			test_in = i;
			
			#100;
		end
		
		$stop;

	end
      
endmodule

