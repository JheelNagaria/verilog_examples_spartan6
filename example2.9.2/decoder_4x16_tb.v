`timescale 1 ps / 1 ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:10:50 09/19/2018
// Design Name:   decoder_4x16
// Module Name:   E:/Jheel/Xilinx Codes/PongChuExamples/example2.9.2/decoder_4x16_tb.v
// Project Name:  example2.9.2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: decoder_4x16
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module decoder_4x16_tb;

	// Inputs
	reg [3:0] test_in;

	// Outputs
	wire [15:0] test_d;

	// Instantiate the Unit Under Test (UUT)
	decoder_4x16 uut (.in(test_in), .d(test_d));
	
	// Loop variables
	integer i = 0;
	
	initial begin
		// Initialize Inputs
		test_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		for (i = 1; i < 16; i = i + 1) begin
			test_in = i;
			
			#100;
		end

		$stop;
        
		// Add stimulus here

	end
      
endmodule

